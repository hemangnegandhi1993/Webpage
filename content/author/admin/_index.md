+++
# Display name
name = "Hemang Negandhi"

# Is this the primary user of the site?
superuser = true

# Role/position
role = "Business Data Analyst at BNP Paribas"
# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
organizations = [ { name = "BNP Paribas", url = "https://group.bnpparibas/en/" } ]

# Short bio (displayed in user profile at end of posts)
bio = "Masters in Computer Science with major in Software Engineering and Databases."

# Enter email to display Gravatar (if Gravatar enabled in Config)
email = "hemangnegandhi1993@gmail.com"

# List (academic) interests or hobbies
interests = [
  "Data Mining",
  "Data Visualization",
  "Databases",
  "Requirement gathering"
]

# List qualifications (such as academic degrees)
[[education.courses]]
  course = "MS in Computer Science"
  institution = "UTA"
  year = 2019

[[education.courses]]
  course = "BE in Information Technology"
  institution = "K.J.Somaiya College of Engineering (Mumbai University)"
  year = 2015



# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.

[[social]]
  icon = "envelope"
  icon_pack = "fas"
  link = "#contact"  # For a direct email link, use "mailto:hemangnegandhi1993@gmail.com"

[[social]]
  icon = "gitlab"
  icon_pack = "fab"
  link = "https://gitlab.com/hemangnegandhi1993"
  
  [[social]]
  icon = "linkedin"
  icon_pack = "fab"
  link = "https://www.linkedin.com/in/hemang-negandhi/"

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
 [[social]]
 icon = "email"
 icon_pack = "ai"
 link = "files/pppt.pdf"
+++

Masters in Computer Science from University of Texas with concentration in Software Engineering and Databases.
Have worked heavily for four years in the domain of Databases, Analytics, Business Intelligence and Software Development.
My proven communication skills and sound understanding of industry best practices offer a productive and contributory presence to both small and large team environments.

Here is summary of the experience I have gained in the past four years:

* Strong Database (RDBMS) development experience in writing queries, functions, stored procedures, triggers using MySQL, MS SQL, Oracle.
* Experience in Data Normalization and Data Modeling (OLTP & OLAP).
* Documenting all ETL projects and work flows.
* Having sound understanding in business logic and resolving the business requirements into software terms that is UML Modeling.
* Have worked heavily on business intelligence tools like Tableau, Powerbi and Qliksense on multiple projects for period of four years.
* Profound and comfortable in client interaction and in requirement gathering, Data cleaning and optimization, ETL process, SDLC.
* Analytical, methodical, and resourceful approach to problem solving, identifying and documenting root causes and corrective actions to meet short and long-term business and system requirements
* Strategic problem-solver and Team worker with interpersonal and communication skills.
