+++
# Feature/Skill widget.
widget = "featurette"  # Do not modify this line!
active = true  # Activate this widget? true/false
title = "Skills"
subtitle = ""
# Order that this section will appear in.
weight = 7
# 
# Add/remove as many `[[feature]]` blocks below as you like.
# 
[[feature]]
  icon = "python"
  icon_pack = "fab"
  name = "Python coding and development"
  description = "90%"
  
 [[feature]]
  icon = "chart-line"
  icon_pack = "fas"
  name = "Statistics"
  description = "100%"  
  
  [[feature]]
   icon = "database"
  icon_pack = "fas"
  name = "Data mining"
  description = "10%"
+++