+++
# Experience widget.
widget = "experience"  # Do not modify this line!
active = true  # Activate this widget? true/false
title = "Job"
subtitle = ""
# Order that this section will appear in.
weight = 8
# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "January 2006"
#   Add/remove as many `[[experience]]` blocks below as you like.

#Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.


[[experience]]
  title = "Business Data Analyst"
  company = "BNP Paribas"
  company_url = "https://www.bnpparibas.co.in/en/"
  location = "Mumbai"
  date_start = "2020-07-27"
  date_end = ""
  description = """
* Hands on experience in Performance Tuning, Query Optimization.
* Created TDEs (Tableau Data Extract) for various Projects and Scheduled Refreshed on Tableau Server.
* Coordinated with onshore team and international stakeholders in Spain to meet business requirement.
* Developed complex workflows and Macros to make data suitable for tableau consumption and reporting. 
* Was responsible for managing huge impact projects in BNP like C3 which generated 422GB of data every month and the output was consumed by more than 10 teams in the organization. 
* Created, developed and maintained tableau dashboards for various projects in BNP."""

[[experience]]
  title = "Intern"
  company = "Depth Consulting"
  company_url = "https://depthconsulting.in/"
  location = "Mumbai"
  date_start = "2020-06-22"
  date_end = "2020-07-10"
  description = """
* Gathered data into oracle analytics from various sources (CSV files, MS SQL, MySQL).
* Built dashboards for measures with forecast, trend line and reference lines.
* Contributed on creating dynamic visualizations in dashboard."""

[[experience]]
  title = "Business Data Analyst"
  company = "BNY Mellon Pershing"
  company_url = "https://www.pershing.com/"
  location = "New Jersey"
  date_start = "2019-09-16"
  date_end = "2020-03-03"
  description = """
* Worked on Alteryx to design and analyze workflows to perform ETL (Data Extraction, Transformation,
Loading).
* Used SQL to extract data and produce ad hoc data visualization reports on the Tableau Server.
* Involved in data validation of the results in Tableau by validating the numbers against the data in the database.
* Involved in creating tableau workbooks for the EPO and the finance department of the company."""

[[experience]]
  title = "Intern"
  company = "AKA Enterprise Solution"
  company_url = "https://www.akaes.com/"
  location = "Newyork"
  date_start = "2018-05-01"
  date_end = "2018-07-01"
  description = """
* Performed migration of inhouse app BI from content workspace to app workspace in powerbi.
* Contributed on creating visualizations for tracking performance of employees.
* Used APIs to fetch Realtime employee data from HR database."""

[[experience]]
  title = "Software Engineer"
  company = "Infogain"
  company_url = "https://www.infogain.com/"
  location = "Mumbai"
  date_start = "2015-08-01"
  date_end = "2017-07-01"
  description = """
* Gathered data into Qliksense from various sources (Oracle, CSV files, MS SQL, MySQL).
* Created dynamic interactive dashboards in Tableau, powerbi and Qliksense.
* Analysed underlying data for potential discrepancies, investigate errors and implemented Incremental load.
* Performed ETL using SQL server Integration Services (SSIS), SQL Server Analysis Services (SSAS), Qlikview.
* Created Real-time streaming dashboard with real-time KPIs in Tableau.
* Documenting all ETL/ELT projects and work flows and including supporting documents for troubleshooting.
* Developed Tableau workbooks to perform YTD, QTD and MTD type of analysis.
* Generated context filters and data source filters while handling huge volume of data in powerbi.
* Built dashboards for measures with forecast, trend line and reference lines.
* Created action filters, parameters and calculated sets for preparing dashboards and worksheets in Tableau."""

[[experience]]
  title = "Intern"
  company = "Cloudfronts Technologies"
  company_url = "https://www.cloudfronts.com/"
  location = "Mumbai"
  date_start = "2015-01-01"
  date_end = "2015-12-31"
  description = """
* Acquired hands-on experience in SSIS, SSRS and PowerBI.
* Demonstrated working with different connectors like Oracle, SQL, Twilio, Zendesk to PowerBI."""

[[experience]]
  title = "Intern"
  company = "Algonation"
  company_url = ""
  location = "Mumbai"
  date_start = "2014-12-01"
  date_end = "2015-02-01"
  description = """
* Understood client requirement and created data model for the application.
* Coded website for the healthcare project using HTML, CSS, PHP and JavaScript.
* Developed a database for storing the healthcare data.
* Worked using bootstrap CSS and Codeignitor framework for PHP."""

[[experience]]
  title = "Intern"
  company = "Trivia Software"
  company_url = ""
  location = "Mumbai"
  date_start = "2014-01-01"
  date_end = "2014-06-01"
  description = """
* Created data model for School ERP solution.
* Coded views and triggers in Oracle SQL using PL-SQL."""
+++
